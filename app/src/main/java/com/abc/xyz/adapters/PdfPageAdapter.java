package com.abc.xyz.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.pdf.PdfDocument;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.abc.xyz.R;
import com.abc.xyz.helper.CanvasView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import top.defaults.colorpicker.ColorPickerPopup;

public class PdfPageAdapter extends RecyclerView.Adapter<PdfPageAdapter.PdfPageViewHolder> {

    private Context context;
    private List<CanvasView> pdfViewList;
    private List<Bitmap> finalBitmaps;
    private Bitmap singleBitmap;


    public PdfPageAdapter(Context context, List<CanvasView> pdfViewList) {
        this.context = context;
        this.pdfViewList = pdfViewList;
        finalBitmaps = new ArrayList<>();
    }

    @NonNull
    @Override
    public PdfPageAdapter.PdfPageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.element_pdf_editor_page, viewGroup, false);
        return new PdfPageViewHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public List<Bitmap> getBitmapPages(){

        for (CanvasView canvasView : pdfViewList){
            finalBitmaps.add(canvasView.getBitmap());
        }
        return finalBitmaps;

    }

    @Override
    public void onBindViewHolder(@NonNull final PdfPageAdapter.PdfPageViewHolder pdfPageViewHolder, final int i) {

        if(pdfViewList.get(i).getParent() != null) {
            ((ViewGroup)pdfViewList.get(i).getParent()).removeView(pdfViewList.get(i));
        }
        pdfPageViewHolder.pdfArea.addView(pdfViewList.get(i));
        pdfViewList.get(i).setPaintStrokeColor(Color.TRANSPARENT);
        pdfViewList.get(i).setPaintStrokeWidth(0);
        pdfViewList.get(i).setOpacity(0);
        pdfViewList.get(i).setPaintFillColor(Color.TRANSPARENT);

        pdfPageViewHolder.marker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new ColorPickerPopup.Builder(context)
                        .initialColor(Color.parseColor("#7cfc00"))
                        .okTitle("Choose")
                        .cancelTitle("Cancel")
                        .showIndicator(true)
                        .showValue(true)
                        .build()
                        .show(pdfPageViewHolder.marker, new ColorPickerPopup.ColorPickerObserver() {
                            @Override
                            public void onColorPicked(int color) {
                                pdfViewList.get(i).setDrawer(CanvasView.Drawer.PEN);
                                pdfViewList.get(i).setPaintStrokeColor(color);
                                pdfViewList.get(i).setOpacity(127);
                                pdfViewList.get(i).setPaintStrokeWidth(15);
                                pdfPageViewHolder.marker.setColorFilter(color);

                            }

                        });

                   pdfPageViewHolder.clear.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {

                           pdfViewList.get(i).clear();
                           pdfPageViewHolder.clear.setColorFilter(Color.BLACK);


                       }
                   });

            }
        });

    }

    @Override
    public int getItemCount() {
        return pdfViewList.size();
    }



   public static class PdfPageViewHolder extends RecyclerView.ViewHolder {

          RelativeLayout pdfArea;
        ImageView clear,  marker;
        private PdfDocument document;


        PdfPageViewHolder(@NonNull View itemView) {
            super(itemView);
            pdfArea = itemView.findViewById(R.id.pdfArea);
            marker = itemView.findViewById(R.id.marker);
            clear = itemView.findViewById(R.id.clearPdf);



        }


/*
        public List<Bitmap> getBitmaps(){



        }
*/

    }
}
