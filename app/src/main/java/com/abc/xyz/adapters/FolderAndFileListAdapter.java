package com.abc.xyz.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.xyz.R;
import com.abc.xyz.activity.MainActivity;
import com.abc.xyz.activity.WritingActivity;
import com.abc.xyz.models.Categories;
import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static android.support.v4.content.ContextCompat.startActivity;


public class FolderAndFileListAdapter extends ExpandableRecyclerAdapter<Categories, String, FolderAndFileListAdapter.FolderList, FolderAndFileListAdapter.FileList> {

    private LayoutInflater layoutInflater;
    private Context context;
    private String folderName;
    private boolean isDrawing;

    public FolderAndFileListAdapter(@NonNull List<Categories> parentList, Context context, boolean isDrawing) {
        super(parentList);
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.isDrawing = isDrawing;
    }

    @NonNull
    @Override
    public FolderList onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View view = layoutInflater.inflate(R.layout.parent_folder, parentViewGroup, false);
        return new FolderList(view);
    }

    @NonNull
    @Override
    public FileList onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View view1 = layoutInflater.inflate(R.layout.child_file_layout, childViewGroup, false);
        return new FileList(view1);
    }

    @Override
    public void onBindParentViewHolder(@NonNull FolderList parentViewHolder, int parentPosition, @NonNull Categories parent) {
        parentViewHolder.bind(parent);
    }

    @Override
    public void onBindChildViewHolder(@NonNull FileList childViewHolder, int parentPosition, int childPosition, @NonNull String child) {
        childViewHolder.bind(child, parentPosition);
    }

    class FolderList extends ParentViewHolder {

        private TextView parentFolderName;
        private ImageView dropDownIcon;

        public FolderList(@NonNull View itemView) {
            super(itemView);
            dropDownIcon = itemView.findViewById(R.id.dropDownIcon);
            parentFolderName = itemView.findViewById(R.id.parentFolderName);

        }

        private void DeleteRecursive(File fileOrDirectory) {
            if (fileOrDirectory.isDirectory())
                for (File child : fileOrDirectory.listFiles()) {
                    child.delete();
                    DeleteRecursive(child);
                }
            fileOrDirectory.delete();
        }

        private void deleteDrawingFolders(String folderName) {
            List<File> files = Arrays.asList(context.getFilesDir().listFiles());
            for (File file : files) {
                if (file.isDirectory()) {
                    if (file.getName().substring(1).equals(folderName)) {
                        DeleteRecursive(file);

                    }
                }
            }
        }

        @Override
        public boolean shouldItemViewClickToggleExpansion() {
            return false;
        }


        public void bind(final Categories categories) {
            parentFolderName.setText(categories.getFolderNames());
            dropDownIcon.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    new AlertDialog.Builder(context)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Are you sure ?")
                            .setMessage("Are u sure u want to delete this folder and all drawings that this folder contains?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    deleteDrawingFolders(categories.getFolderNames());
                                    notifyDataSetChanged();
                                }
                            })
                            .setNegativeButton("No", null).show();


                    return true;
                }
            });


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isExpanded()) {
                        dropDownIcon.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);
                        collapseView();
                    } else {
                        dropDownIcon.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp);
                        expandView();

                    }
                }
            });
        }


    }


    class FileList extends ChildViewHolder {

        private TextView fileNametv;


        public FileList(@NonNull View itemView) {
            super(itemView);
            fileNametv = itemView.findViewById(R.id.fileNametv);
        }

        private void deleteDrawingFiles(String fileName, String folderName) {

            List<File> files = Arrays.asList(context.getFilesDir().listFiles());
            for (File file : files) {
                if (file.isDirectory()) {
                    if (file.getName().substring(1).equals(folderName)) {
                        List<File> fileunderFolder = Arrays.asList(file.listFiles());
                        for (File file2 : fileunderFolder) {
                            if (file2.isFile()) {
                                String fileFinalName = file2.getName().substring(2);
                                if (fileFinalName.equals(fileName)) {
                                    boolean b = file2.delete();
                                }
                            }
                        }
                    }
                }
            }
        }

        public void bind(final String secondCategory, final int parentPosition) {
            fileNametv.setText(secondCategory);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    new AlertDialog.Builder(context)
                            .setTitle("Select Operation")
                            .setItems(new String[]{"Share", "Delete"}, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case 0:
                                            extShareDrawingFile(secondCategory, getParentList().get(parentPosition).getFolderNames());
                                            break;
                                        case 1:
                                            new AlertDialog.Builder(context)
                                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                                    .setTitle("Are you sure ?")
                                                    .setMessage("Are u sure u want to delete this note?")
                                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            Log.wtf("secondCategory", secondCategory);
                                                            deleteDrawingFiles(secondCategory, getParentList().get(parentPosition).getFolderNames());
                                                            notifyDataSetChanged();
                                                        }
                                                    })
                                                    .setNegativeButton("No", null).show();
                                            break;
                                    }
                                }
                            }).show();
                    return true;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (isDrawing) {
                        Intent catalogueIntent = new Intent(context, MainActivity.class);
                        catalogueIntent.putExtra("categoryName", secondCategory);
                        catalogueIntent.putExtra("folderName", getParentList().get(parentPosition).getFolderNames());
                        startActivity(context, catalogueIntent, null);

                    } else {
                        Intent catalogueIntent = new Intent(context, WritingActivity.class);
                        catalogueIntent.putExtra("categoryName", secondCategory);
                        catalogueIntent.putExtra("folderName", getParentList().get(parentPosition).getFolderNames());
                        startActivity(context, catalogueIntent, null);
                    }

                }
            });
        }

        private void extShareDrawingFile(String fileName, String folderName) {
            List<File> files = Arrays.asList(context.getFilesDir().listFiles());
            for (File file : files) {
                if (file.isDirectory()) {
                    if (file.getName().substring(1).equals(folderName)) {
                        List<File> fileunderFolder = Arrays.asList(file.listFiles());
                        for (File file2 : fileunderFolder) {
                            if (file2.isFile()) {
                                String fileFinalName = file2.getName().substring(1);
                                if (fileFinalName.equals(fileName)) {
                                    Intent intentShareFile = new Intent(Intent.ACTION_SEND);
                                    intentShareFile.setType("application/pdf");
                                    intentShareFile.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file2));
                                    intentShareFile.putExtra(Intent.EXTRA_SUBJECT, "Sharing " + fileName);
                                    intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing " + fileName + " with you. Please have a look at it.");
                                    intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    startActivity(context, Intent.createChooser(intentShareFile, "Share " + fileName + " using..."), null);
                                }
                            }
                        }
                    }
                }
            }
        }


        private void intShareDrawingFile(String fileName, String folderName) {
            List<File> files = Arrays.asList(context.getFilesDir().listFiles());
            for (File file : files) {
                if (file.isDirectory()) {
                    if (file.getName().substring(1).equals(folderName)) {
                        List<File> fileunderFolder = Arrays.asList(file.listFiles());
                        for (File file2 : fileunderFolder) {
                            if (file2.isFile()) {
                                String fileFinalName = file2.getName().substring(1);
                                if (fileFinalName.equals(fileName)) {
                                    /* TODO: Share 'file2' using internal shareit like wifi sharer
                                     *
                                     */
                                }
                            }
                        }
                    }
                }
            }
        }

    }

}
