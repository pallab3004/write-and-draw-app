package com.abc.xyz.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abc.xyz.R;
import com.abc.xyz.activity.PdfReaderActivity;

import java.io.File;
import java.util.List;

public class PdfAdapter extends RecyclerView.Adapter<PdfAdapter.PdfSingleHolder> {

    private Context context;
    private List<File> pdfFiles;
    private PdfDocument document;

    public PdfAdapter(Context context, List<File> pdfFiles) {

        this.context = context;
        this.pdfFiles = pdfFiles;
    }


    @NonNull
    @Override
    public PdfSingleHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.single_pdf, viewGroup, false);
        return new PdfSingleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PdfSingleHolder pdfSingleHolder, final int i) {

        pdfSingleHolder.PdfName.setText(pdfFiles.get(i).getName());
        pdfSingleHolder.singlePdfRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent pdfopen = new Intent(context, PdfReaderActivity.class);
                pdfopen.putExtra("PdfFile", pdfFiles.get(i));
                context.startActivity(pdfopen);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pdfFiles.size();
    }

    class PdfSingleHolder extends RecyclerView.ViewHolder {

        private ConstraintLayout singlePdfRl;
        private TextView PdfName;

        public PdfSingleHolder(@NonNull View itemView) {
            super(itemView);
            singlePdfRl = itemView.findViewById(R.id.singlePdfRl);
            PdfName = itemView.findViewById(R.id.PdfName);
        }
    }
}
