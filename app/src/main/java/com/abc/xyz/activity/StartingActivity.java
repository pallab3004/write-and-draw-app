package com.abc.xyz.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.xyz.R;
import com.abc.xyz.adapters.FolderAndFileListAdapter;
import com.abc.xyz.adapters.PdfAdapter;
import com.abc.xyz.helper.AppConstants;
import com.abc.xyz.helper.PdfFileListAsyncTask;
import com.abc.xyz.models.Categories;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StartingActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {



    private CardView writePage, drawPage, pdfActivity;
    private PdfFileListAsyncTask pdfAsyncTask;
    private FolderAndFileListAdapter fileListAdapter;
    private RecyclerView savedItemList;
    private RelativeLayout listOfFolders;
    private TextView new_item;
    private int activityJumpingFlag;
    private PdfAdapter pdfAdapter;
    private ImageView noFileAvailable;


    private void setUpToolbar() {
        Toolbar startingToolBar = findViewById(R.id.startingToolbar);
        startingToolBar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(startingToolBar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("My Notes");
    }


    private void intialise() {
        noFileAvailable = findViewById(R.id.noFileAvailable);
        writePage = findViewById(R.id.writePage);
        drawPage = findViewById(R.id.drawPage);
        savedItemList = findViewById(R.id.savedItemList);
        pdfActivity = findViewById(R.id.pdfActivity);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        savedItemList.setLayoutManager(linearLayoutManager);
        listOfFolders = findViewById(R.id.listOfFolders);
        listOfFolders.setVisibility(View.GONE);
        new_item = findViewById(R.id.new_item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);
        setUpToolbar();
        intialise();
    }


    private void setFileListAdapterDrawing() {
        List<Categories> categoriesList = new ArrayList<>();
        File[] folders = getApplicationContext().getFilesDir().listFiles();
        for (File file : folders) {
            String firstFolderLetter = String.valueOf(file.getName().charAt(0));
            if (file.isDirectory() && firstFolderLetter.equals("d")) {
                List<String> file1 = new ArrayList<>();
                File[] files = file.listFiles();
                for (File file2 : files) {
                    if (file2.isFile()) {
                        String first = String.valueOf(file2.getName().charAt(0));
                        if (first.equals("d")) {
                            String fileFinalName = file2.getName().substring(1);
                            file1.add(fileFinalName);
                        }

                    }
                }
                Categories categories = new Categories(file1, file.getName().substring(1));
                categoriesList.add(categories);
            }
            if(categoriesList.isEmpty()){
                savedItemList.setAdapter(null);
                noFileAvailable.setVisibility(View.VISIBLE);
            }else {
                fileListAdapter = new FolderAndFileListAdapter(categoriesList, StartingActivity.this, true);
                savedItemList.setAdapter(fileListAdapter);
                noFileAvailable.setVisibility(View.GONE);

            }
               }
    }

    private void setFileListAdapterWriting() {
        List<Categories> categoriesList = new ArrayList<>();
        File[] folders = getApplicationContext().getFilesDir().listFiles();
        for (File file : folders) {
            String firstFolderLetter = String.valueOf(file.getName().charAt(0));
            if (file.isDirectory() && firstFolderLetter.equals("w")) {
                List<String> file1 = new ArrayList<>();
                File[] files = file.listFiles();
                for (File file2 : files) {
                    if (file2.isFile()) {
                        String first = String.valueOf(file2.getName().charAt(0));
                        if (first.equals("w")) {
                            String fileFinalName = file2.getName().substring(2);
                            file1.add(fileFinalName);
                        }
                    }
                }
                Categories categories = new Categories(file1, file.getName().substring(1));
                categoriesList.add(categories);
            }
            if(categoriesList.isEmpty()){
                savedItemList.setAdapter(null);
                noFileAvailable.setVisibility(View.VISIBLE);
            }else {
                fileListAdapter = new FolderAndFileListAdapter(categoriesList, StartingActivity.this, false);
                savedItemList.setAdapter(fileListAdapter);
                noFileAvailable.setVisibility(View.GONE);

            }

        }
    }



    private void paperListShow() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.background_paper_list, null, false);
        builder.setView(view);
        ListView listView = view.findViewById(R.id.backgroundPaperList);
        ImageView close = view.findViewById(R.id.closePaperListDialog);
        ArrayList<String> i = new ArrayList<>();
        i.add("Plain paper");
        i.add("Simple Graph paper");
        i.add("Axis Graph paper");
        i.add("Ruled paper");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(StartingActivity.this, android.R.layout.simple_expandable_list_item_1, i);
        listView.setAdapter(arrayAdapter);
        final AlertDialog dialog = builder.show();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (parent.getAdapter().getItem(position).toString()) {

                    case "Plain paper":

                        Intent writeIntent = new Intent(StartingActivity.this, WritingActivity.class);
                        writeIntent.putExtra("categoryName", "");
                        writeIntent.putExtra("folderName", "");
                        writeIntent.putExtra("backgroundPaper", AppConstants.PaperCodes.PLAIN_PAPER_CODE);
                        startActivity(writeIntent);
                        dialog.dismiss();
                        break;
                    case "Simple Graph paper":
                        Intent writeIntentSimpleGraph = new Intent(StartingActivity.this, WritingActivity.class);
                        writeIntentSimpleGraph.putExtra("categoryName", "");
                        writeIntentSimpleGraph.putExtra("folderName", "");
                        writeIntentSimpleGraph.putExtra("backgroundPaper", AppConstants.PaperCodes.SIMPLE_GRAPH_PAPER_CODE);
                        startActivity(writeIntentSimpleGraph);
                        dialog.dismiss();
                        break;
                    case "Axis Graph paper":
                        Intent writeIntentAxisGraph = new Intent(StartingActivity.this, WritingActivity.class);
                        writeIntentAxisGraph.putExtra("categoryName", "");
                        writeIntentAxisGraph.putExtra("folderName", "");
                        writeIntentAxisGraph.putExtra("backgroundPaper", AppConstants.PaperCodes.AXIS_GRAPH_PAPER_CODE);
                        startActivity(writeIntentAxisGraph);
                        dialog.dismiss();
                        break;

                    case "Ruled paper":

                        Intent writeIntentRuledPaper = new Intent(StartingActivity.this, WritingActivity.class);
                        writeIntentRuledPaper.putExtra("categoryName", "");
                        writeIntentRuledPaper.putExtra("folderName", "");
                        writeIntentRuledPaper.putExtra("backgroundPaper", AppConstants.PaperCodes.RULED_PAPER_CODE);
                        startActivity(writeIntentRuledPaper);
                        dialog.dismiss();
                        break;

                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }



    @Override
    protected void onResume() {
        super.onResume();

        writePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                savedItemList.setAdapter(null);
                setFileListAdapterWriting();
                listOfFolders.setVisibility(View.VISIBLE);
                new_item.setVisibility(View.VISIBLE);
                activityJumpingFlag = 1;

            }
        });

        drawPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedItemList.setAdapter(null);
                setFileListAdapterDrawing();
                listOfFolders.setVisibility(View.VISIBLE);
                new_item.setVisibility(View.VISIBLE);
                activityJumpingFlag = 0;
            }
        });


        new_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (activityJumpingFlag == 0) {
                    Intent drwaIntent = new Intent(StartingActivity.this, MainActivity.class);
                    drwaIntent.putExtra("categoryName", "");
                    drwaIntent.putExtra("folderName", "");
                    startActivity(drwaIntent);
                }

                if (activityJumpingFlag == 1) {

                    paperListShow();

                }
            }
        });


        pdfActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dexter.withActivity(StartingActivity.this)
                        .withPermissions(
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    savedItemList.setAdapter(null);
                                    pdfAsyncTask = new PdfFileListAsyncTask();
                                    pdfAsyncTask.setListener(new PdfFileListAsyncTask.PdfFileListAsyncTaskListener() {
                                        @Override
                                        public void onPdfFileListAsyncTaskFinished(List<File> files) {
                                            pdfAdapter = new PdfAdapter(StartingActivity.this, files);
                                            if(files.isEmpty()||files == null){

                                                savedItemList.setAdapter(null);
                                                noFileAvailable.setVisibility(View.VISIBLE);
                                            }else {

                                                savedItemList.setAdapter(pdfAdapter);
                                                pdfAdapter.notifyDataSetChanged();
                                                noFileAvailable.setVisibility(View.GONE);
                                            }

                                        }
                                    });
                                    pdfAsyncTask.execute(getApplicationContext().getFilesDir(), Environment.getExternalStorageDirectory());
                                    new_item.setVisibility(View.GONE);
                                    listOfFolders.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();

            }
        });
    }
}
