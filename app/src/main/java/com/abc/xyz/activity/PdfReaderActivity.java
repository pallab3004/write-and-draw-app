package com.abc.xyz.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.xyz.R;
import com.abc.xyz.adapters.PdfPageAdapter;
import com.abc.xyz.helper.CanvasView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PdfReaderActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String STATE_CURRENT_PAGE_INDEX = "current_page_index";
    int pageCount;
    TextView currentPosition, totalItems;
    private PdfPageAdapter pageAdapter;
    private PdfRenderer mPdfRenderer;
    private ParcelFileDescriptor mFileDescriptor;
    private PdfRenderer.Page mCurrentPage;
    private Button mButtonPrevious;
    private Button mButtonNext;
    private int mPageIndex = 0;
    private ImageView save;
    private CanvasView pdfPagesRecycler;
    private List<Bitmap> bitmapList,bitmapsToBeSaved;
    private PdfDocument document;
    private RecyclerView pdfRecycler;
    private List<CanvasView> pdfViewList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_reader);
        initialise();
    }

    private void initialise() {
        document = new PdfDocument();
        save = findViewById(R.id.savePDf);
        bitmapsToBeSaved = new ArrayList<>();
        // pdfPagesRecycler = findViewById(R.id.pdfPagesRecycler);
        //pdfViewList = new ArrayList<>();
        pdfRecycler = findViewById(R.id.pdfPagesRecycler);
        pdfRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }

            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        });
        mButtonNext = findViewById(R.id.nextButton);
        mButtonPrevious = findViewById(R.id.prevButton);
        // Bind events.
        mButtonPrevious.setOnClickListener(this);
        mButtonNext.setOnClickListener(this);
        save.setOnClickListener(this);
        currentPosition = findViewById(R.id.currentPage);
        totalItems = findViewById(R.id.totalPage);
        try {
            initialiseRenderer();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.prevButton: {
                showPage(mPageIndex = mPageIndex - 1);
                break;
            }
            case R.id.nextButton: {
                showPage(mPageIndex = mPageIndex + 1);
                break;
            }

            case R.id.savePDf: {
                saveImagePdf(onSaveButtonTap());
                Toast.makeText(PdfReaderActivity.this, "PDF Saved", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }

    @Override
    public void onStop() {
        try {
            closeRenderer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (null != mCurrentPage) {
            outState.putInt(STATE_CURRENT_PAGE_INDEX, mCurrentPage.getIndex());
        }
    }


    private List<Bitmap> onSaveButtonTap() {
        return pageAdapter.getBitmapPages();
    }


    private void saveImagePdf(Bitmap bitmap) {
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(bitmap.getWidth(), bitmap.getHeight(), 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#ffffff"));
        canvas.drawPaint(paint);
        canvas.drawBitmap(bitmap, 0, 0, null);
        document.finishPage(page);
        writeNewImage(document, "TestPdfFolderA", "TestDFileA");
    }

    private void saveImagePdf(List<Bitmap> bitmapPages) {

        for (Bitmap bitmap : bitmapPages) {
            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(bitmap.getWidth(), bitmap.getHeight(), (bitmapPages.indexOf(bitmap) + 1)).create();
            PdfDocument.Page page = document.startPage(pageInfo);
            Canvas canvas = page.getCanvas();
            Paint paint = new Paint();
            paint.setColor(Color.parseColor("#ffffff"));
            canvas.drawPaint(paint);
            canvas.drawBitmap(bitmap, 0, 0, null);
            document.finishPage(page);
        }
        writeNewImage(document, "TestPdfFolderA", "complete45");
    }

    private void writeNewImage(PdfDocument pdfDocument, String folderName, String fileName) {
        File folder = new File(getApplicationContext().getFilesDir(), "d" + folderName);
        boolean b = folder.mkdirs();
        try {
            pdfDocument.writeTo(new FileOutputStream(new File(folder, "d" + fileName + ".pdf")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showPage(int index) {
        pdfRecycler.scrollToPosition(index);
        // pdfPagesRecycler.drawBitmap(bitmapList.get(index));
        currentPosition.setText(String.valueOf(index + 1));
        totalItems.setText(String.valueOf(mPdfRenderer.getPageCount()));
        mButtonPrevious.setEnabled(0 != index);
        mButtonNext.setEnabled(index + 1 < pageCount);
    }

    private void initialiseRenderer() throws IOException {
        // In this sample, we read a PDF from the assets directory.
        mFileDescriptor = ParcelFileDescriptor.open((File) Objects.requireNonNull(getIntent().getExtras()).get("PdfFile"), ParcelFileDescriptor.MODE_READ_WRITE);
        // This is the PdfRenderer we use to render the PDF.
        if (mFileDescriptor != null) {
            mPdfRenderer = new PdfRenderer(mFileDescriptor);
        }
        pageCount = mPdfRenderer.getPageCount();
        pdfViewList = new ArrayList<>(pageCount);
        CanvasView pdfV;
        for (int i = 0; i < pageCount; i++) {

            pdfV = new CanvasView(PdfReaderActivity.this);
            if (null != mCurrentPage) {
                mCurrentPage.close();
            }
            // Use `openPage` to open a specific page in PDF.
            mCurrentPage = mPdfRenderer.openPage(i);
            // Important: the destination bitmap must be ARGB (not RGB).
            //  Bitmap bitmap = BitmapFactory.decodeFile(path, option);

            Bitmap bitmap = Bitmap.createBitmap(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels,
                    Bitmap.Config.ARGB_8888);
            // Here, we render the page onto the Bitmap.
            // To render a portion of the page, use the second and third parameter. Pass nulls to get
            // the default result.
            // Pass either RENDER_MODE_FOR_DISPLAY or RENDER_MODE_FOR_PRINT for the last parameter.
            mCurrentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            //pdfPagesRecycler.drawBitmap(bitmap);
            pdfV.drawBitmap(bitmap);
            pdfViewList.add(pdfV);
        }

        pageAdapter = new PdfPageAdapter(PdfReaderActivity.this, pdfViewList);
        pdfRecycler.setAdapter(pageAdapter);
        showPage(mPageIndex);
    }

    private void closeRenderer() throws IOException {
        if (null != mCurrentPage) {
            mCurrentPage.close();
        }
        mPdfRenderer.close();
        mFileDescriptor.close();
    }

}
