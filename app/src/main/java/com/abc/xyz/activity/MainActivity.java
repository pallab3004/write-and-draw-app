package com.abc.xyz.activity;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.abc.xyz.R;
import com.abc.xyz.adapters.SpinnerAdapter;
import com.abc.xyz.helper.CanvasView;
import com.google.common.io.Files;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import top.defaults.colorpicker.ColorPickerPopup;

public class MainActivity extends AppCompatActivity {

    private CanvasView drawArea;
    private ImageView pencil, brush, eraser, undo, redo, save, clear_drawing, ruler1, zoomIn, zoomOut,dragScreen;
    private View dialogView;
    private Spinner folderSpinner;
    private String getFileName, getFolderName;
    private PdfDocument document;
    private float originalDrawAreaX , originalDrawAreaY;
    private boolean canDrag = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawArea = findViewById(R.id.drawArea);
        drawArea.setMode(CanvasView.Mode.DRAW);
        setupToolBar();
        initialise();
        pencil.setColorFilter(Color.WHITE);
        drawArea.setPaintStrokeColor(Color.LTGRAY);
    }


    private void setupToolBar() {

        Toolbar mainDrawToolBar = findViewById(R.id.mainDrawToolBar);
        setSupportActionBar(mainDrawToolBar);
    }


    private void initialise() {

        dragScreen = findViewById(R.id.dragScreenAfterZoom);
        originalDrawAreaX = drawArea.getScaleX();
        originalDrawAreaY = drawArea.getScaleY();
        document = new PdfDocument();
        zoomIn = findViewById(R.id.zoomIn);
        zoomOut = findViewById(R.id.zoomOut);
        pencil = findViewById(R.id.pencil);
        brush = findViewById(R.id.brush);
        eraser = findViewById(R.id.eraser);
        undo = findViewById(R.id.undo);
        ruler1 = findViewById(R.id.ruler1);
        redo = findViewById(R.id.redo);
        clear_drawing = findViewById(R.id.clear_drawing);
        save = findViewById(R.id.save);
        getFileName = getIntent().getStringExtra("categoryName");
        getFolderName = getIntent().getStringExtra("folderName");
        if (!(getFileName.isEmpty()) && !(getFolderName.isEmpty())) {
            ShowImagefetched();
        }
    }


    private void ShowImagefetched() {

        List<File> folders = Arrays.asList(getApplicationContext().getFilesDir().listFiles());
        for (File file : folders) {
            if (file.isDirectory()) {
                List<File> files = Arrays.asList(file.listFiles());
                for (File file2 : files) {
                    if (file2.isFile()) {
                        String fileNamegot = file2.getName().substring(1);
                        if (fileNamegot.equals(getFileName)) {
                            if (fileNamegot.endsWith("pdf")) {
                                Toast.makeText(getApplicationContext(), "Inside PDF Open", Toast.LENGTH_SHORT).show();
                                try {
                                    PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(file2.getAbsoluteFile(), ParcelFileDescriptor.MODE_READ_ONLY));
                                    PdfRenderer.Page page = renderer.openPage(0);
                                    Bitmap bitmap = Bitmap.createBitmap(page.getWidth(), page.getHeight(),
                                            Bitmap.Config.ARGB_8888);
                                    page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                                    drawArea.drawBitmap(bitmap);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Inside NORMAL Open", Toast.LENGTH_LONG).show();
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                                Bitmap bitmap = BitmapFactory.decodeFile(file2.getAbsolutePath(), options);
                                drawArea.drawBitmap(bitmap);
                            }
                        }
                    }
                }
            }
        }
    }

    private void saveImage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        dialogView = inflater.inflate(R.layout.dialog_save_drawing, null, false);
        populateExistingFolders();
        final EditText fileNameEditText = dialogView.findViewById(R.id.file_name);
        final EditText newFolder = dialogView.findViewById(R.id.new_folder_name);
        final Spinner existingFolder = dialogView.findViewById(R.id.existing_folder_name);
        if (!(getFolderName.isEmpty()) && !(getFileName.isEmpty())) {
            newFolder.setEnabled(false);
            existingFolder.setSelection(folderList().indexOf(getFolderName));
            fileNameEditText.setText(Files.getNameWithoutExtension(getFileName));
        } else {
            newFolder.setEnabled(true);
        }
        builder.setTitle("Save Drawing")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //List<Bitmap> bitmapPages = new ArrayList<>(Collections.singleton());
                        String folderName = resolveFolderName(newFolder, existingFolder);
                        String fileName = fileNameEditText.getText().toString();
                        if (validateSaveOptions(drawArea.getBitmap(), folderName, fileName)) {
                            if (checkForOverwrite(folderName,fileName)) {
                                writeNewImage(drawArea.getBitmap(), folderName, fileName);
                                Toast.makeText(getApplicationContext(), "Image Saved as PDF", Toast.LENGTH_SHORT).show();

                            } else {
                                overWriteImage(drawArea.getBitmap(), folderName, fileName);
                                Toast.makeText(getApplicationContext(), "Image Overwitten as PDF", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(MainActivity.this, "Save Cancelled", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                })
                .setView(dialogView);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }


    private boolean checkForOverwrite(String FolderName, String FileName){
        boolean newFile = true;
       if((getFolderName==null || getFolderName.isEmpty())&& (getFileName==null || getFileName.isEmpty())){
           newFile = true;
       }else if((FileName.equals(getFileName.substring(0,getFileName.indexOf(".")))) && (getFolderName.equals(FolderName))) {
           newFile = false;
       }
        return  newFile;
    }

    private String resolveFolderName(EditText newFolder, Spinner existingFolder) {
        String ret = null;
        if (existingFolder.getSelectedItem() != null && !existingFolder.getSelectedItem().toString().contentEquals("Existing Folder"))
            ret = existingFolder.getSelectedItem().toString();
        else if (!newFolder.getText().toString().isEmpty() || existingFolder.getSelectedItem().toString().contentEquals("Existing Folder"))
            ret = newFolder.getText().toString();
        else if (newFolder.getText().toString().isEmpty() || existingFolder.getSelectedItem().toString().contentEquals("Existing Folder")) {
            ret = "";
        } else if (!newFolder.getText().toString().isEmpty() || !existingFolder.getSelectedItem().toString().contentEquals("Existing Folder")) {
            ret = "NA";

        }
        return ret;
    }

    private boolean validateSaveOptions(Bitmap bitmaps, String folderName, String fileName) {

        boolean validated = true;

        if (folderName == null || folderName.isEmpty()) {
            Toast.makeText(MainActivity.this, "Please select or name a new folder", Toast.LENGTH_SHORT).show();
            validated = false;
        } else if (folderName.equals("NA")) {
            Toast.makeText(MainActivity.this, "Please choose either Existing folder or New Folder , both can't be selected", Toast.LENGTH_LONG).show();
            validated = false;
        } else if (fileName.isEmpty()) {
            Toast.makeText(MainActivity.this, "Please provide a file name", Toast.LENGTH_SHORT).show();
            validated = false;
        }
        return validated;

    }

    private void overWriteImage(Bitmap bitmapPages, String folderName, String fileName) {
        List<File> files = Arrays.asList(getApplicationContext().getFilesDir().listFiles());
        for (File file : files) {
            if (file.isDirectory()) {
                String firstFolderChar = String.valueOf(file.getName().charAt(0));
                if (firstFolderChar.equals("d")) {
                    if (file.getName().substring(1).equals(folderName)) {
                        List<File> fileunderFolder = Arrays.asList(file.listFiles());
                        for (File file2 : fileunderFolder) {
                            if (file2.isFile()) {
                                String first = String.valueOf(file2.getName().charAt(0));
                                if (first.equals("d")) {
                                    String fileFinalName = file2.getName().substring(1);
                                    if (fileName.equals(fileFinalName.substring(0,fileFinalName.indexOf(".")))) {

                                            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(bitmapPages.getWidth(), bitmapPages.getHeight(), 1).create();
                                            PdfDocument.Page page = document.startPage(pageInfo);
                                            Canvas canvas = page.getCanvas();
                                            Paint paint = new Paint();
                                            paint.setColor(Color.parseColor("#ffffff"));
                                            canvas.drawPaint(paint);
                                            canvas.drawBitmap(bitmapPages, 0, 0, null);
                                            document.finishPage(page);
                                        try {
                                            document.writeTo(new FileOutputStream(file2));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void writeNewImage(Bitmap bitmapPages, String folderName, String fileName) {
        File folder = new File(getApplicationContext().getFilesDir(), "d" + folderName);
        boolean b = folder.mkdirs();
        document = new PdfDocument();
            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(bitmapPages.getWidth(), bitmapPages.getHeight(),  1).create();
            PdfDocument.Page page = document.startPage(pageInfo);
            Canvas canvas = page.getCanvas();
            Paint paint = new Paint();
            paint.setColor(Color.parseColor("#ffffff"));
            canvas.drawPaint(paint);
            canvas.drawBitmap(bitmapPages, 0, 0, null);
            document.finishPage(page);

        try {
            document.writeTo(new FileOutputStream(new File(folder, "d" + fileName + ".pdf")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void populateExistingFolders() {
        folderSpinner = dialogView.findViewById(R.id.existing_folder_name);
        List<String> categories = folderList();
        SpinnerAdapter dataAdapter = new SpinnerAdapter(getApplicationContext(), android.R.layout.simple_spinner_item);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataAdapter.addAll(categories);
        dataAdapter.add("Existing Folder");
        folderSpinner.setAdapter(dataAdapter);
        folderSpinner.setSelection(dataAdapter.getCount());
    }

    @Override
    protected void onResume() {
        super.onResume();

        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                float x = drawArea.getScaleX();
                float y = drawArea.getScaleY();

                if(x < 6.0f && y < 6.0f){
                    drawArea.setScaleX(x+1);
                    drawArea.setScaleY(y+1);
                    drawArea.setScale((int) x);

                }

            }
        });


        dragScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(canDrag){
                    drawArea.setDragMode(true);
                    dragScreen.setColorFilter(Color.WHITE);
                    brush.setColorFilter(Color.BLACK);
                    pencil.setColorFilter(Color.BLACK);
                    ruler1.setColorFilter(Color.BLACK);
                    eraser.setColorFilter(Color.BLACK);
                }else {

                    Toast.makeText(MainActivity.this, "Canvas needs to zoomed out for dragging", Toast.LENGTH_SHORT).show();
                }
                    }
        });

        zoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                float x = drawArea.getScaleX();
                float y = drawArea.getScaleY();

                if(x > 1.0f && y > 1.0f){
                    canDrag = true;
                    drawArea.setScaleX(x-1);
                    drawArea.setScaleY(y-1);
                    drawArea.setScale((int) x);
                }else {
                    canDrag = false;
                }

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveImage();
            }
        });

        clear_drawing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Clear Drawing", Toast.LENGTH_SHORT).show();
                drawArea.clear();
            }
        });

        pencil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawArea.setDragMode(false);
                drawArea.setDrawer(CanvasView.Drawer.PEN);
                drawArea.setPaintStrokeColor(Color.LTGRAY);
                brush.setColorFilter(Color.BLACK);
                pencil.setColorFilter(Color.WHITE);
                ruler1.setColorFilter(Color.BLACK);
                eraser.setColorFilter(Color.BLACK);
                dragScreen.setColorFilter(Color.BLACK);
            }
        });

        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawArea.undo();
            }
        });


        eraser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawArea.setDragMode(false);
                dragScreen.setColorFilter(Color.BLACK);
                drawArea.setDrawer(CanvasView.Drawer.PEN);
                eraser.setColorFilter(Color.WHITE);
                drawArea.setPaintStrokeColor(Color.WHITE);
                drawArea.setPaintStrokeWidth(20);
                pencil.setColorFilter(Color.BLACK);
                brush.setColorFilter(Color.BLACK);
                ruler1.setColorFilter(Color.BLACK);
            }
        });


        redo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawArea.redo();

            }
        });

        brush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawArea.setDragMode(false);
                dragScreen.setColorFilter(Color.BLACK);
                new ColorPickerPopup.Builder(MainActivity.this)
                        .initialColor(Color.RED) // Set initial color
                        .enableBrightness(true) // Enable brightness slider or not
                        .enableAlpha(true) // Enable alpha slider or not
                        .okTitle("Choose")
                        .cancelTitle("Cancel")
                        .showIndicator(true)
                        .showValue(true)
                        .build()
                        .show(brush, new ColorPickerPopup.ColorPickerObserver() {
                            @Override
                            public void onColorPicked(int color) {
                                drawArea.setDrawer(CanvasView.Drawer.PEN);
                                brush.setColorFilter(color);
                                drawArea.setPaintStrokeColor(color);
                                pencil.setColorFilter(Color.BLACK);
                                eraser.setColorFilter(Color.BLACK);
                                ruler1.setColorFilter(Color.BLACK);
                            }

                        });
            }
        });

        ruler1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawArea.setDragMode(false);
                dragScreen.setColorFilter(Color.BLACK);
                drawArea.setDrawer(CanvasView.Drawer.LINE);
                new ColorPickerPopup.Builder(MainActivity.this)
                        .initialColor(Color.RED) // Set initial color
                        .enableBrightness(true) // Enable brightness slider or not
                        .enableAlpha(true) // Enable alpha slider or not
                        .okTitle("Choose")
                        .cancelTitle("Cancel")
                        .showIndicator(true)
                        .showValue(true)
                        .build()
                        .show(ruler1, new ColorPickerPopup.ColorPickerObserver() {
                            @Override
                            public void onColorPicked(int color) {
                                ruler1.setColorFilter(color);
                                drawArea.setPaintStrokeColor(color);
                                brush.setColorFilter(Color.BLACK);
                                pencil.setColorFilter(Color.BLACK);
                                eraser.setColorFilter(Color.BLACK);
                            }

                        });
            }
        });

    }

    private List<String> folderList() {
        List<String> folders = new ArrayList<String>();
        List<File> files = Arrays.asList(getApplicationContext().getFilesDir().listFiles());
        for (File file : files) {
            if (file.isDirectory()) {
                String firstFolderChar = String.valueOf(file.getName().charAt(0));
                if (firstFolderChar.equals("d")) {
                    folders.add(file.getName().substring(1));
                }
            }
        }
        return folders;
    }
}
