package com.abc.xyz.helper;

public class AppConstants {

    public static class PaperCodes{
        public final static int PLAIN_PAPER_CODE = 0;
        public final static int RULED_PAPER_CODE = 1;
        public final static int SIMPLE_GRAPH_PAPER_CODE = 2;
        public final static int AXIS_GRAPH_PAPER_CODE = 3;

    }



}
