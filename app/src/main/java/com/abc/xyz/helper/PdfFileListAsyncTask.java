package com.abc.xyz.helper;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class PdfFileListAsyncTask extends AsyncTask<File, Integer, List<File>> {

    private PdfFileListAsyncTaskListener listener;
    private List<File> pdfFiles = new ArrayList<>();

    @Override
    protected List<File> doInBackground(File... roots) {
        for (File root : roots)
            walk(root);
        return pdfFiles;
    }

    @Override
    protected void onPostExecute(List<File> files) {
        super.onPostExecute(files);
        if (listener != null) {
            listener.onPdfFileListAsyncTaskFinished(files);
        }
    }

    public void setListener(PdfFileListAsyncTaskListener listener) {
        this.listener = listener;
    }

    private void walk(File root) {
        File[] list = root.listFiles();

        for (File f : list) {
            if (f.isDirectory()) {
                walk(f);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    try {
                        String fileType = Files.probeContentType(f.toPath());
                        if (fileType != null && fileType.contains("pdf")) {
                            pdfFiles.add(f);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (f.getName().endsWith(".pdf")) {
                        pdfFiles.add(f);
                    }
                }
            }
        }
    }

    public interface PdfFileListAsyncTaskListener {
        void onPdfFileListAsyncTaskFinished(List<File> files);
    }
}