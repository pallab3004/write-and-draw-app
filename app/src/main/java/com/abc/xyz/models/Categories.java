package com.abc.xyz.models;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.List;

public class Categories implements Parent<String> {

    private List<String> filenames;
    private String folderNames;

    public List<String> getFilenames() {
        return filenames;
    }

    public String getFolderNames() {
        return folderNames;
    }


    public void setFilenames(List<String> filenames) {
        this.filenames = filenames;
    }

    public void setFolderNames(String folderNames) {
        this.folderNames = folderNames;
    }

    public Categories(List<String> filenames, String folderNames) {
        this.filenames = filenames;
        this.folderNames = folderNames;
    }

    @Override
    public List<String> getChildList() {
        return filenames;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
